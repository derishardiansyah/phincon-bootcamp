import React, { useState } from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { setTheme } from '../../theme/actions';
import { useDispatch } from 'react-redux';
import { selectTheme } from '../../theme/selector';
import { selectTodo, selectTodoFilter, selectTodoActive, selectTodoEdit } from '../../ToDo/selector';
import { setTodo, setTodoClear, setTodoFilter, setTodoActive, setTodoRemove, setTodoEdit } from '../../ToDo/actions';
import { IconButton, Paper, InputBase } from '@mui/material';
import uuid from 'react-uuid';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import EditIcon from '@mui/icons-material/Edit';
import CloseIcon from '@mui/icons-material/Close';
import CheckCircleIcon from '@mui/icons-material/CheckCircle'; // Import the check mark icon
import Tooltip from '@mui/material/Tooltip';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import classes from '@components/Content/style.module.scss';

function Content({ theme, todo, todoFilter }) {
  const dispatch = useDispatch();
  const [editItemId, setEditItemId] = useState(null);

  const isLight = theme === 'dark';

  const handleTheme = () => {
    if (theme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      const ObjTodo = {
        id: uuid(),
        data: event.target.value,
        status: 'active',
      };

      dispatch(setTodo(ObjTodo));
      event.target.value = '';

      localStorage.setItem('todo', JSON.stringify([...todo, ObjTodo]));
    }
  };

  const handleFilter = (selectedFilter) => {
    dispatch(setTodoFilter(selectedFilter));
  };

  const handleActive = (id) => {
    const toggleActive = todo.map((todo) =>
      todo.id === id
        ? {
            ...todo,
            status: todo.status === 'active' ? 'completed' : 'active',
          }
        : todo
    );

    dispatch(setTodoActive(toggleActive));
  };

  const handleEdit = (id) => {
    // Fungsi untuk mengaktifkan mode edit pada todo tertentu
    // Mengatur id item yang akan diedit, atau mematikan mode edit jika id yang sama sudah aktif
    if (editItemId === id) {
      setEditItemId(null); // Matikan mode edit jika id yang sama sudah aktif
    } else {
      setEditItemId(id); // Aktifkan mode edit dengan mengatur id item yang akan diedit
    }
  };

  const handleEditChange = (id, value) => {
    const editedTodo = todo.map((todoItem) => (todoItem.id === id ? { ...todoItem, data: value } : todoItem));

    dispatch(setTodoEdit(editedTodo));
  };

  const handleRemoveTodo = (id) => {
    const remove = todo.filter((todo) => todo.id !== id);

    dispatch(setTodoRemove(remove));
  };

  const handleClearCompleted = (clearcompleted) => {
    const clear = todo.filter((todo) => todo.status !== clearcompleted);

    dispatch(setTodoClear(clear));
  };

  let filteredTodos;
  if (todoFilter === 'active') {
    filteredTodos = todo.filter((item) => item.status === 'active');
  } else if (todoFilter === 'completed') {
    filteredTodos = todo.filter((item) => item.status === 'completed');
  } else {
    filteredTodos = todo;
  }

  const completedCount = filteredTodos ? filteredTodos.reduce((count) => (count += 1), 0) : 0;

  const myTheme = createTheme({
    palette: {
      background: {
        default: isLight ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isLight ? 'hsl(237, 14%, 26%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isLight ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  const radioButtonUncheckedIconColor = isLight ? 'white' : 'black';

  return (
    <ThemeProvider theme={myTheme}>
      <div className={isLight ? classes.containerDark : classes.container}>
        <div className={isLight ? classes.pageUpDark : classes.pageUp}>
          <div className={classes.content}>
            <div className={classes.header}>
              <div className={classes.leftHeader}>
                <h1>TODO</h1>
              </div>
              <div className={classes.rightHeader} variant="contained" onClick={handleTheme}>
                <IconButton>
                  {theme === 'light' ? (
                    <NightsStayIcon sx={{ fontSize: 36, color: 'black' }} />
                  ) : (
                    <WbSunnyIcon sx={{ fontSize: 36, color: 'white' }} />
                  )}
                </IconButton>
              </div>
            </div>
            <div className={classes.input}>
              <Paper component="form" sx={{ p: '0.6rem 1rem', display: 'flex', alignItems: 'center' }}>
                <IconButton>
                  <RadioButtonUncheckedIcon style={{ color: radioButtonUncheckedIconColor }} />
                </IconButton>
                <InputBase
                  sx={{ ml: 1, flex: 1, fontFamily: `'Josefin Sans', sans-serif`, fontSize: '26px' }}
                  placeholder="Currently Typing"
                  onKeyDown={handleKeyDown}
                />
              </Paper>
            </div>
            <br />
            <div className={classes.display}>
              {filteredTodos?.length > 0 && (
                <Paper sx={{ p: '0.6rem 1rem', alignItems: 'center' }} style={{ maxHeight: 350, overflow: 'auto' }}>
                  {filteredTodos?.map((item) => (
                    <div className={isLight ? classes.displayTextDark : classes.displayText} key={item.id}>
                      <div className={classes.itemContainer}>
                        {editItemId === item.id ? (
                          <InputBase
                            value={item.data}
                            onChange={(e) => handleEditChange(item.id, e.target.value)}
                            autoFocus
                          />
                        ) : (
                          <>
                            <Tooltip title={item.status === 'active' ? 'Completed' : 'Active'}>
                              <IconButton onClick={() => handleActive(item.id)}>
                                {item.status === 'completed' ? (
                                  <RadioButtonCheckedIcon style={{ color: 'red' }} />
                                ) : (
                                  <RadioButtonUncheckedIcon style={{ color: radioButtonUncheckedIconColor }} />
                                )}
                              </IconButton>
                            </Tooltip>
                            <div
                              className={item.status === 'completed' ? classes.valueActive : classes.value}
                              onDoubleClick={() => handleEdit(item.id)}
                            >
                              {item.data}
                            </div>
                          </>
                        )}
                      </div>
                      <div className={classes.ubah}>
                        <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleEdit(item.id)}>
                          {editItemId === item.id ? (
                            <CheckCircleIcon sx={{ color: 'green', fontSize: '22px' }} />
                          ) : (
                            <EditIcon sx={{ color: 'grey', fontSize: '22px' }} />
                          )}
                        </IconButton>
                        <IconButton sx={{ p: '10px' }} aria-label="menu" onClick={() => handleRemoveTodo(item.id)}>
                          <CloseIcon sx={{ color: 'grey', fontSize: '22px' }} />
                        </IconButton>
                      </div>
                    </div>
                  ))}
                </Paper>
              )}
            </div>

            <div className={classes.footerContent}>
              <Paper
                sx={{
                  p: '10px',
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  fontSize: '15px',
                }}
              >
                <div className={classes.footerLeft}>{completedCount} Item left</div>
                <div className={classes.footerMiddle}>
                  <div className={classes.middleOne} onClick={() => handleFilter('all')}>
                    All
                  </div>
                  <div className={classes.middleOne} onClick={() => handleFilter('active')}>
                    Active
                  </div>
                  <div className={classes.middleOne} onClick={() => handleFilter('completed')}>
                    Completed
                  </div>
                </div>
                <div className={classes.clearCompleted} onClick={() => handleClearCompleted('completed')}>
                  Clear Completed
                </div>
              </Paper>
            </div>

            <div className={classes.footerDragDrop}>
              <h4 className={classes.nameDragDrop}>Drag and drop to reorder list </h4>
            </div>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}

Content.propTypes = {
  theme: PropTypes.string,
  todo: PropTypes.array,
  todoFilter: PropTypes.string,
  todoActive: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  todo: selectTodo,
  todoFilter: selectTodoFilter,
  todoActive: selectTodoActive,
  todoEdit: selectTodoEdit,
});

export default connect(mapStateToProps)(Content);
