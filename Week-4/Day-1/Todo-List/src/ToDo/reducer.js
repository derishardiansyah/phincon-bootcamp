import { produce } from 'immer';

import {
  SET_TODO,
  SET_TODO_CLEAR,
  SET_TODO_FILTER,
  SET_TODO_ACTIVE,
  SET_TODO_REMOVE,
  SET_TODO_EDIT,
} from '../ToDo/constant';

export const initialState = {
  todo: [],
  todoFilter: 'all',
};

export const storedKey = ['todo'];

const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TODO:
        draft.todo = [...draft.todo, action.todo];
        break;
      case SET_TODO_FILTER:
        draft.todoFilter = action.todoFilter;
        break;
      case SET_TODO_REMOVE:
        draft.todo = action.todoRemove;
        break;
      case SET_TODO_ACTIVE:
        draft.todo = action.todoActive;
        break;
      case SET_TODO_EDIT:
        draft.todo = action.todoEdit;
        break;
      case SET_TODO_CLEAR:
        draft.todo = action.todoClear;
        break;
      default:
        break;
    }
  });

export default todoReducer;
