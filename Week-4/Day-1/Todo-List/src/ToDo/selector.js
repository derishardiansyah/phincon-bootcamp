import { createSelector } from 'reselect';

import { initialState } from '../ToDo/reducer';

const selectTodoState = (state) => state.todo || initialState;

// export const selectLocale = createSelector(selectLanguage, (state) => state.locale);
export const selectTodo = createSelector(selectTodoState, (state) => state.todo);
export const selectTodoFilter = createSelector(selectTodoState, (state) => state.todoFilter);
export const selectTodoActive = createSelector(selectTodoState, (state) => state.todoActive);
export const selectTodoEdit = createSelector(selectTodoState, (state) => state.todoEdit);
