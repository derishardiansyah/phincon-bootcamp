import {
  SET_TODO,
  SET_TODO_CLEAR,
  SET_TODO_FILTER,
  SET_TODO_ACTIVE,
  SET_TODO_REMOVE,
  SET_TODO_EDIT,
} from '../ToDo/constant';

export function setTodo(todo) {
  return {
    type: SET_TODO,
    todo,
  };
}

export const setTodoFilter = (todoFilter) => ({
  type: SET_TODO_FILTER,
  todoFilter,
});

export const setTodoActive = (todoActive) => ({
  type: SET_TODO_ACTIVE,
  todoActive,
});

export function setTodoRemove(todoRemove) {
  return {
    type: SET_TODO_REMOVE,
    todoRemove,
  };
}

export function setTodoEdit(todoEdit) {
  return {
    type: SET_TODO_EDIT,
    todoEdit,
  };
}

export function setTodoClear(todoClear) {
  return {
    type: SET_TODO_CLEAR,
    todoClear,
  };
}
