import { all } from 'redux-saga/effects';

import appSaga from '@containers/App/saga';
import adviceSaga from '@containers/AdviceGenerator/saga';

export default function* rootSaga() {
  yield all([appSaga(), adviceSaga()]);
}
