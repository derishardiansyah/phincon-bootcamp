import axios from 'axios';

const urls = {
  advicegenerator: 'https://api.adviceslip.com/advice',
};

export const callAPI = async (endpoint, method, headers = {}, params = {}, data = {}) => {
  const options = {
    url: endpoint,
    method,
    headers,
    data,
    params,
  };

  return axios(options).then((response) => {
    const responseAPI = response.data;
    return responseAPI;
  });
};

export const getAdviceGenerator = () =>
  callAPI(urls.advicegenerator, 'get', { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
