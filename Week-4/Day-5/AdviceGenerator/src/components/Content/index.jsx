import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { getAdviceAction } from '@containers/AdviceGenerator/action';
import { selectAdvice, selectLoading } from '@containers/AdviceGenerator/selector';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { setTheme } from '../../containers/theme/actions';
import { selectTheme } from '../../containers/theme/selector';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import classes from '@components/Content/style.module.scss';
import logoRefresh from '@static/images/refresh-btn.svg';
import IconButton from '@mui/material/IconButton';
import Skeleton from '@mui/material/Skeleton';
import { TypeAnimation } from 'react-type-animation';

const Content = ({ theme, advice, loading }) => {
  const dispatch = useDispatch();
  const isLight = theme === 'dark';

  const handleTheme = () => {
    if (theme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };

  const myTheme = createTheme({
    palette: {
      background: {
        default: isLight ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isLight ? 'hsl(237, 14%, 26%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isLight ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  useEffect(() => {
    dispatch(getAdviceAction());
  }, []);

  const handleRefresh = () => {
    dispatch(getAdviceAction());
  };

  return (
    <ThemeProvider theme={myTheme}>
      <div className={isLight ? classes.containerDark : classes.container}>
        <div className={isLight ? classes.boxDark : classes.box}>
          <div className={classes.rightHeader} variant="contained" onClick={handleTheme}>
            <IconButton>
              {theme === 'light' ? (
                <NightsStayIcon sx={{ fontSize: 36, color: 'black' }} />
              ) : (
                <WbSunnyIcon sx={{ fontSize: 36, color: 'white' }} />
              )}
            </IconButton>
          </div>

          {advice && (
            <>
              <div className={isLight ? classes.fieldIdDark : classes.fieldId}>
                {loading ? (
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      variant="text"
                      animation="wave"
                      width={120}
                      className={isLight ? classes.fieldIdDark : classes.fieldId}
                      // style={{ margin: '10px 0',  }}
                    />
                  </div>
                ) : (
                  <TypeAnimation sequence={[`ADVICE #${advice.id}`, 1000]} speed={50} repeat={Infinity} />
                )}
              </div>
              <div className={isLight ? classes.fieldContentDark : classes.fieldContent}>
                {loading ? (
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'center',
                      textAlign: 'center',
                      justifyContent: 'center',
                    }}
                  >
                    <Skeleton
                      variant="text"
                      animation="wave"
                      width="85%"
                      height={100}
                      className={isLight ? classes.fieldContentDark : classes.fieldContent}
                      style={{ marginTop: '-40px' }}
                    />
                  </div>
                ) : (
                  <TypeAnimation sequence={[`"${advice.advice}"`, 1000]} speed={50} repeat={Infinity} />
                )}
              </div>
            </>
          )}

          <div className={classes.fieldQoute}>
            <div className={classes.line}></div>
            <svg xmlns="http://www.w3.org/2000/svg" width="444" height="1" viewBox="0 0 444 1" fill="none">
              <path fillRule="evenodd" clipRule="evenodd" d="M0 0H196V1H0V0ZM248 0H444V1H248V0Z" fill="#4E5D73" />
            </svg>

            <div className={classes.qoute}>
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16" fill="none">
                <path
                  d="M6 3C6 1.34315 4.65685 0 3 0C1.34315 0 0 1.34315 0 3V13C0 14.6569 1.34315 16 3 16C4.65685 16 6 14.6569 6 13V3Z"
                  fill={isLight ? '#CEE3E9' : '#000000'}
                />
                <path
                  d="M20 3C20 1.34315 18.6569 0 17 0C15.3431 0 14 1.34315 14 3V13C14 14.6569 15.3431 16 17 16C18.6569 16 20 14.6569 20 13V3Z"
                  fill={isLight ? '#CEE3E9' : '#000000'}
                />
              </svg>
            </div>
          </div>

          <div className={classes.fieldGenerate}>
            <IconButton onClick={handleRefresh}>
              <img src={logoRefresh} alt="" />
            </IconButton>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
};

Content.propTypes = {
  theme: PropTypes.string,
  advice: PropTypes.object,
  loading: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
  advice: selectAdvice,
  loading: selectLoading,
});

export default connect(mapStateToProps)(Content);
