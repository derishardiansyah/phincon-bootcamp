import { createSelector } from 'reselect';

import { initialState } from './reducer';

const selectThemeState = (state) => state.theme || initialState;

export const selectTheme = createSelector(selectThemeState, (state) => state.theme);
