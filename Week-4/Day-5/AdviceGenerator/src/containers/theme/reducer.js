import { produce } from 'immer';
import { SET_THEME } from './constant';

export const initialState = {
  theme: 'light',
};

export const storedKey = [];

const themeReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_THEME:
        draft.theme = action.theme;
        break;
      default:
        break;
    }
  });

export default themeReducer;
