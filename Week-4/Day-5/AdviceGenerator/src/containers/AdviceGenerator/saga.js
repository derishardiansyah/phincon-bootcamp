import { takeLatest, call, put } from 'redux-saga/effects';

import { GET_ADVICE } from './constant';
import { setAdviceAction, setAdviceLoading } from './action';
import { getAdviceGenerator } from '@domain/api';

export function* doGetAdviceGenerator() {
  try {
    const response = yield call(getAdviceGenerator);
    yield put(setAdviceAction(response.slip));
  } catch (error) {
    console.log(error);
  }
  yield put(setAdviceLoading(false));
}

export default function* adviceSaga() {
  yield takeLatest(GET_ADVICE, doGetAdviceGenerator);
}
