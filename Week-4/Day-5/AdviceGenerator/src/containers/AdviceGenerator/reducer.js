import { produce } from 'immer';
import { SET_ADVICE_STATE, SET_ADVICE_LOADING, GET_ADVICE } from './constant';

export const initialState = {
  advice: null,
  loading: false,
};

export const storedKeyQuote = ['advice'];

const adviceReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case GET_ADVICE:
        draft.loading = true;
        break;
      case SET_ADVICE_STATE:
        draft.advice = action.advice;
        draft.loading = false;
        break;
      case SET_ADVICE_LOADING:
        draft.loading = action.loading;
        break;
      default:
        break;
    }
  });

export default adviceReducer;
