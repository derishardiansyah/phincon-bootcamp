import { GET_ADVICE, SET_ADVICE_STATE, SET_ADVICE_LOADING } from './constant';

export const getAdviceAction = () => ({
  type: GET_ADVICE,
});

export const setAdviceAction = (advice) => ({
  type: SET_ADVICE_STATE,
  advice,
});

export const setAdviceLoading = (loading) => ({
  type: SET_ADVICE_LOADING,
  loading,
});
