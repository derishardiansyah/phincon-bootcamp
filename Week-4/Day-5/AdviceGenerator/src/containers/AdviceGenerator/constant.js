export const GET_ADVICE = 'advice/GET_ADVICE';
export const SET_ADVICE_LOADING = 'advice/SET_ADVICE_LOADING';
export const SET_ADVICE_STATE = 'advice/SET_ADVICE_STATE';
