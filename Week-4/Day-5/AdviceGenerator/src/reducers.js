import { combineReducers } from 'redux';

import appReducer, { storedKey as storedAppState } from '@containers/App/reducer';
import languageReducer from '@containers/Language/reducer';
import themeReducer, { storedKey as storedThemeState } from './containers/theme/reducer';
import adviceReducer from '@containers/AdviceGenerator/reducer';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  app: { reducer: appReducer, whitelist: storedAppState },
  theme: { reducer: themeReducer, whitelist: storedThemeState },
  advice: { reducer: adviceReducer, whitelist: storedThemeState },
};

const temporaryReducers = {
  language: languageReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
