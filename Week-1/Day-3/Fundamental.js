// Sum to Number
    function myFunction (a, b){

        console.log(a+b) 
    }
    myFunction(1,2)

// Comparison Data
    function myFunction(a, b) {
        console.log( a === b)
    }
    myFunction(2, 3)

// Get Data
    function myFunction(a) {
            console.log(typeof(a))
        }
        myFunction(1)

// Remove first n
    function myFunction(a) {
        console.log(a.slice(3))
    }
    myFunction('abcdefg')

// Get last N
    function myFunction(str) {
        console.log(str.slice(-3))
    }
    myFunction('abcdefg')

// Get first
    function myFunction(a) {
        console.log(a.slice(0,3))
    }
    myFunction('abcdefg')

// Find the position
    function myFunction(a) {
        console.log(a.indexOf('is'))
    }
    myFunction("praise")

// Extract first half
    function myFunction(a) {
        console.log(a.slice(0, a.length / 2))        
    }
    myFunction('abcdefgh')

// Remove last N
    function myFunction(a) {
        console.log(a.slice(0,-3))
    }
    myFunction('abcdefg')

// Return the percentage
    function myFunction(a,b) {
        console.log(b / 100 * a)
    }
    myFunction(100,50)

// Basic Javascript math operator
    function myFunction(a, b, c, d, e, f) {
    console.log( (((a + b - c) * d) / e) ** f)
    }
    myFunction(6,5,4,3,2,1)

// Check whether a string contains another string and concatenate
    function myFunction(a, b) {
    console.log( a.indexOf(b) === -1 ? a + b : b + a)
    }
    myFunction('cheese', 'cake')

// Check if a number is even
    function myFunction(a) {
        console.log( a % 2 === 0)
    }
    myFunction(10)

// How many times does a character occur
    function myFunction(a, b) {
        console.log( b.split(a).length - 1)
    }
    myFunction('m', 'how many times does the character occur in this sentence?')

// Check if a number is a whole number
    function myFunction(a) {
        console.log( parseInt(a) === a)
    }
    myFunction(4)

// Multiplication, division, and comparison operators
        function myFunction(a, b) {
        console.log( a < b ? a / b : a * b)
    }
    myFunction(10, 100)
// Round a number to 2 decimal places
    function myFunction(a) {
        console.log( Number(a.toFixed(2)))
    }
    myFunction(2.12397)
// Split a number into its digits
    function myFunction( a ) {
        const string = a + '';
        const strings = string.split('');
        console.log( strings.map(digit => Number(digit)))
    }
    myFunction(10)