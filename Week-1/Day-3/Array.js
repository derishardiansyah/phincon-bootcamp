// Get nth element of array
    function myFunction(a, n) {
        console.log( a[n - 1])
    }
    myFunction([1,2,3,4,5],3)

// Remove first n elements of an array
    function myFunction(a) {
        console.log( a.slice(3))
    }
    myFunction([1,2,3,4])

// Get last n elements of an array
    function myFunction(a) {
        console.log( a.slice(-3))
    }
    myFunction([1,2,3,4])

// Get first n elements of an array
    function myFunction(a) {
        console.log( a.slice(0, 3))
    }
    myFunction([1,2,3,4])

// Return last n array elements
    function myFunction(a, n) {
        console.log( a.slice(-n))
    }
    myFunction([1, 2, 3, 4, 5], 2)

// Remove a specific array element
    function myFunction( a, b ) {
        console.log( a.filter(arrItem => arrItem !== b))
    }
    myFunction([1,2,3], 2)
// Count number of elements in JavaScript array
    function myFunction(a) {
        console.log( a.length)
    }
    myFunction([1,2,2,4])

// Count number of negative values in array
    function myFunction(a) {
        console.log( a.filter((el) => el < 0).length)
    }
    myFunction([1,-2,2,-4])

// Sort an array of strings alphabetically
    function myFunction(arr) {
        console.log( arr.sort())
    }
    myFunction([1,3,2])

// Calculate the sum of an array of numbers
    function myFunction(a) {
        console.log( a.reduce((acc, cur) => acc + cur, 0))
    }
    myFunction([10,100,40])

// Return the average of an array of numbers
    function myFunction( arr ) {
        console.log( arr.reduce((acc, cur) => acc + cur, 0) / arr.length)
    }
    myFunction([10,100,40])

// Return the longest string from an array of strings
    function myFunction( arr ) {
        console.log( arr.reduce((a, b) => a.length <= b.length ? b : a))
    }
    myFunction(['help', 'me'])

// Check if all array elements are equal
    function myFunction( arr ) {
        console.log( new Set(arr).size === 1)
    }
    myFunction([true, true, true, true])

// Merge an arbitrary number of arrays
    function myFunction(...arrays) {
        console.log( arrays.flat())
    }
    myFunction([1, 2, 3], [4, 5, 6])

// Sort array by object property
    function myFunction(arr) {

        const sort = (x, y) => x.b - y.b
        console.log( arr.sort(sort))
    }
    myFunction([{a:1,b:2},{a:5,b:4}])

// Merge two arrays with duplicate values
    function myFunction(a, b) {
        let merged = a.concat(b);
        let noDuplicate = Array.from(new Set(merged))
        let sorted = noDuplicate.sort((a, b) => a - b)
        console.log( sorted)
    }
    myFunction([1, 2, 3], [3, 4, 5])