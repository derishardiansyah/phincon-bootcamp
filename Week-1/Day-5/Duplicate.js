const duplicate = (a, b) => {
  let gabung = a.concat(b);
  let noDuplicate = Array.from(new Set(gabung)).filter((value) => value !== undefined);
  return noDuplicate; 
};

console.log(duplicate([1, 0, 1, 0])); // Output: [1, 0]
console.log(duplicate(["John", "Taylor", "John"])); // Output: ["John", "Taylor"]
