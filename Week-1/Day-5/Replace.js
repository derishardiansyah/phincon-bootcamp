const emotify = (kalimat) => {
  const emoticons = {
    smile: ":D",
    grin: ":)",
    sad: ":(",
    mad: ":P",
  };

  return kalimat.replace(/\b(smile|grin|sad|mad)\b/g, (match) => emoticons[match]);
};

console.log(emotify("Make me smile"));  // Output: "Make me :D"
console.log(emotify("Make me grin"));  // Output: "Make me :)"
console.log(emotify("Make me sad"));  // Output: "Make me :("
console.log(emotify("Make me mad"));  
