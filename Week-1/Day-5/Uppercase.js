const uppercase = (a) => {
  return a.charAt(0).toUpperCase() + a.slice(1);
};

console.log(uppercase("This is a title"));  
