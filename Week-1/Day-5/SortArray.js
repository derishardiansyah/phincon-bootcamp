const SortArray = (data) => {
    const array = Object.entries(data).sort((a,b) => b[1] - a[1]);
    
    const hasil = {};
    for (let i=0; i<array.length;i++){
        const [nama] = array[i];
        if (i === 0){
            hasil[nama] = "Gold";
        } else if (i===1) {
            hasil[nama] = "Silver";
        } else if (i === 2) {
            hasil[nama] = "Bronze";
        } else {
            hasil[nama] = "Participants";
        }
    }
    return hasil;

}

console.table(SortArray({
  "Joshua" : 45,
  "Alex" : 39,
  "Eric" : 43
}));

console.table(SortArray({
	'Person A' : 1,
	'Person B' : 2,
	'Person C' : 3,
	'Person D' : 4,
	'Person E' : 102
}));