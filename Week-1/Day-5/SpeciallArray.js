function isSpecialArray(arr) {
  return arr.every((item, index) => item % 2 === index % 2);
}
        
console.log(isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3])); // Output: true