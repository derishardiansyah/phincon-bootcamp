const operation = (x, y, pilih) => {
  x = parseFloat(x);
  y = parseFloat(y);

  if (pilih === "add") {
    return x + y;
  } else if (pilih === "subtract") {
    return x - y;
  } else if (pilih === "divide") {
    if (y === 0) {
      return "Undefined";
    }
    return x / y;
  } else if (pilih === "multiply") {
    return x * y;
  } 
};

console.log(operation("6",  "0",  "divide"));  // Output: 3
console.log(operation("1",  "2",  "add" ));
console.log(operation("4",  "5",  "subtract"))
console.log(operation("6",  "3",  "divide"))
console.log(operation("2",  "7",  "multiply"))
console.log(operation("6",  "0",  "divide"));
