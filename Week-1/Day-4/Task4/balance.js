const balanced = (word) => {
  const middleIndex = Math.floor(word.length / 2);
  let a = 0;
  let b = 0;

    // charCodeAt melihat nilai key huruf pada keyboard
  for (let i = 0; i < word.length; i++) {
    if (i < middleIndex) {
      a += word.charCodeAt(i);
    } else if (i > middleIndex) {
      b += word.charCodeAt(i);
    }
  }

  return a === b;
};

console.log(balanced("zips"));  
console.log(balanced("brake"));  
console.log(balanced("at"));  
