const leftSlide = (row) => {

  // Menggabungkan nilai yang sama
  for (let i = 0; i < row.length - 1; i++) {
    if (row[i] === row[i + 1]) {
      row[i] *= 2;
    //   membuat elemen array baru dengan data aslinya
      row.splice(i + 1, 1); 
    }
  }

  // melalukan looping menggunakan while dengan nilai 4 karena dalam game sisinya 4x4
  while (row.length < 4) {
    row.push(0);
  }

  return row;
};

console.log(leftSlide([2, 2, 2, 0]));