import fetch from 'node-fetch';

const getData = async () => {
    const url = "https://api.jikan.moe/v4/anime";

    const response = await fetch(url);
    const data = await response.json();
    const data_split = data.data.slice(0, 5);

    // Nomor 1
    // const data_all = data_split.map((item) => item.entry).flat();
    const AllTitle = data_split.map((item) => item.title);
    
    console.log("Title",AllTitle)

    // Nomor 2
    const data_Date = data_split.sort((a, b) => new Date (a.aired.from) - new Date (b.aired.from))
    const data_byDate = data_Date.map((item) => item.title);
    console.log("DataByDate",data_byDate);

    // Nomor 3
    const data_Popularity = data_split.sort((a, b) => b.popularity - a.popularity);
    const data_byPopularity = data_Popularity.map((item) => item.title + " " + ":" + " " +item.popularity);
    console.log("DataByPopularity",data_byPopularity);

    // Nomor 4
    const data_Rank = data_split.sort((a, b) => a.rank - b.rank);
    const data_byRank = data_Rank.map((item) => item.title + " " + ":" + " " +item.rank);
    console.log("DataByRank",data_byRank);

    // Nomor 5
    const data_Episodes = data_split.sort((a,b) => b.episodes - a.episodes);
    const data_byEpisodes = data_Episodes.map((item) => item.title + " " + ":" + " " + item.episodes);
    console.log("DataMostEpisode",data_byEpisodes)

}

export default getData;