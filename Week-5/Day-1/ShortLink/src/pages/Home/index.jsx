import Navbar from '@components/Navbar';
import ContentUp from '@components/ContentUp';
import ContentBottom from '@components/ContentBottom';
import ContentLast from '@components/ContentLast';
import Footer from '@components/Footer';

const Home = () => (
  <div>
    <Navbar />
    <ContentUp />
    <ContentBottom />
    <ContentLast />
    <Footer />
  </div>
);

export default Home;
