import { Button } from '@mui/material';
import { useTranslation } from 'react-i18next';

import study from '@static/images/working.svg';
import classes from './style.module.scss';

const ContentUp = () => {
  const { t } = useTranslation();
  return (
    <div className={classes.container}>
      <div className={classes.contentup}>
        <div className={classes.contentLeft}>
          <div className={classes.nameContent}>{t('h2')}</div>
          <div className={classes.subtittle}>{t('text')}</div>
          <div className={classes.getStarted}>
            <Button variant="contained">{t('get')}</Button>
          </div>
        </div>
        <div className={classes.contentRight}>
          <img src={study} alt="" />
        </div>
      </div>
    </div>
  );
};

export default ContentUp;
