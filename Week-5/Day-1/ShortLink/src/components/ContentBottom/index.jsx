import PropTypes from 'prop-types';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import InputBase from '@mui/material/InputBase';
import { Button } from '@mui/material';
import { useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useTranslation } from 'react-i18next';
import { selectShortUrl, selectShortUrlLoading, selectShortUrlError } from '@containers/Shortlink/selector';
import { getShortUrl, deleteShortUrl } from '@containers/Shortlink/action';

import roundedOne from '@static/images/icon-brand-recognition.svg';
import roundedTwo from '@static/images/icon-detailed-records.svg';
import roundedThree from '@static/images/icon-fully-customizable.svg';
import backgroundInput from '@static/images/bg-shorten-desktop.svg';
import Loader from '@components/Loader';
import classes from './style.module.scss';

const ContentBottom = ({ shortUrl, shortUrlLoading, shortUrlError }) => {
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  const [copiedIndex, setCopiedIndex] = useState(null);
  const { t } = useTranslation();

  const handleInputUrlShort = (event) => {
    setValue(event.target.value);
  };

  const handleSubmitUrlShort = (e) => {
    e.preventDefault();
    if (shortUrl.includes(value)) {
      // eslint-disable-next-line no-undef
      setDuplicateError(true);
    } else {
      dispatch(getShortUrl(value));
      setValue('');
    }
  };

  const handleCopyButtonClick = (short, index) => {
    navigator.clipboard.writeText(short.full_short_link);
    setCopiedIndex(index);
    setTimeout(() => {
      setCopiedIndex(null);
    }, 700);
  };

  const handleDelete = (code) => {
    dispatch(deleteShortUrl(code));
  };

  const handleError = () => {
    switch (shortUrlError.errorCode) {
      case 1:
        return t('ec1');
      case 2:
        return t('ec2');
      case 10:
        return t('ec10');
      case 11:
        return t('ec11');
      default:
        return t('eccdefault');
    }
  };

  return (
    <div className={classes.container}>
      <div className={classes.content}>
        <div className={classes.middle}>
          <div className={classes.boxInput} style={{ backgroundImage: `url(${backgroundInput})` }}>
            <Paper
              className={classes.paperr}
              component="form"
              sx={{
                p: '2px 4px',
                display: 'flex',
                alignItems: 'center',
                backgroundColor: 'white',
                maxLength: '10',
              }}
            >
              <InputBase
                sx={{ ml: 1, flex: 1, color: 'black', fontSize: '15px' }}
                placeholder={t('placeholder')}
                value={value}
                onChange={handleInputUrlShort}
              />
            </Paper>
            <div className={classes.inputButton}>
              <Button variant="contained" onClick={handleSubmitUrlShort}>
                {t('shorten')}
              </Button>
            </div>
            {shortUrlError && <div className={classes.error}>{handleError()}</div>}
          </div>
        </div>

        {shortUrl?.map((short, index) => (
          <div key={short.code}>
            <div className={classes.result}>
              <div className={classes.boxResult}>
                <div className={classes.resultLeft}>
                  <div className={classes.resultOriginalLink}>{short?.original_link}</div>
                  <div className={classes.linee}>
                    <hr />
                  </div>
                  <div className={classes.resultShortLink}>{short?.full_short_link}</div>
                </div>

                <div className={classes.resultRight}>
                  <div>
                    <Button
                      variant="contained"
                      className={copiedIndex === index ? classes.copiedButton : classes.buttonCopy}
                      onClick={() => handleCopyButtonClick(short, index)}
                    >
                      {copiedIndex === index ? t('copied') : t('copy')}
                    </Button>
                  </div>
                  <div>
                    <Button className={classes.delete} variant="contained" onClick={() => handleDelete(short.code)}>
                      <DeleteIcon />
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}

        <Loader isLoading={shortUrlLoading} />

        <div className={classes.contentbottom}>
          <div className={classes.bottomText}>
            <div className={classes.textOne}>{t('ad')}</div>
            <div className={classes.textTwo}>{t('textad')}</div>
          </div>
          <div className={classes.bottomCard}>
            <div className={classes.cardOne}>
              <div className={classes.box}>
                <div className={classes.rounded}>
                  <img src={roundedOne} alt="" />
                </div>
                <div className={classes.nameCard}>{t('title1')}</div>
                <div className={classes.detailName}>{t('text1')}</div>
              </div>
            </div>
            <div className={classes.cardTwo}>
              <div className={classes.box}>
                <div className={classes.rounded}>
                  <img src={roundedTwo} alt="" />
                </div>
                <div className={classes.nameCard}>{t('title2')}</div>
                <div className={classes.detailName}>{t('text2')}</div>
              </div>
            </div>
            <div className={classes.cardThree}>
              <div className={classes.box}>
                <div className={classes.rounded}>
                  <img src={roundedThree} alt="" />
                </div>
                <div className={classes.nameCard}>{t('title3')}</div>
                <div className={classes.detailName}>{t('text3')}</div>
              </div>
            </div>
            <div className={classes.line} />
          </div>
        </div>
      </div>
    </div>
  );
};

ContentBottom.propTypes = {
  shortUrl: PropTypes.array,
  shortUrlLoading: PropTypes.bool,
  shortUrlError: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  shortUrl: selectShortUrl,
  shortUrlLoading: selectShortUrlLoading,
  shortUrlError: selectShortUrlError,
});

export default connect(mapStateToProps)(ContentBottom);
