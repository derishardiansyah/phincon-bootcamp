import { Button } from '@mui/material';
import { useTranslation } from 'react-i18next';

import background from '@static/images/bg-boost-desktop.svg';
import classes from './style.module.scss';

const ContentLast = () => {
  const { t } = useTranslation();
  return (
    <div className={classes.container}>
      <div className={classes.contentLast} style={{ backgroundImage: `url(${background})` }}>
        <div className={classes.nameContent}>{t('boost')}</div>
        <div className={classes.getStarted}>
          <Button variant="contained">{t('get')}</Button>
        </div>
      </div>
    </div>
  );
};

export default ContentLast;
