import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ReorderIcon from '@mui/icons-material/Reorder';
import CloseIcon from '@mui/icons-material/Close';
import Button from '@mui/material/Button';

import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Menu, MenuButton, MenuItem, MenuList } from '@chakra-ui/react';
import { setToggleNavbar } from '@containers/Navbar/action';
import { selectToggleNavbar } from '@containers/Navbar/selector';

import idFlag from '@static/images/id.svg';
import usFlag from '@static/images/us.svg';
import classes from './navbar.module.scss';

const Navbar = ({ toggleNavbar }) => {
  const dispatch = useDispatch();
  const [isMobileOpen, setIsMobileOpen] = useState(false);
  const { t, i18n } = useTranslation();
  const [selectedLanguage, setSelectedLanguage] = useState('en');

  const handleToggle = () => {
    dispatch(setToggleNavbar(!toggleNavbar));
    setIsMobileOpen(!isMobileOpen);
  };
  const handleLanguageChange = (language) => {
    setSelectedLanguage(language);
    i18n.changeLanguage(language);
  };

  return (
    <div className={classes.container}>
      <div className={classes.navbar}>
        <div className={classes.nameNavbar}>
          <h3>Shortly</h3>
        </div>

        <div className={classes.toggleDesktop}>
          <div className={classes.fiturNavbar}>
            <div className={classes.fiturOne}>{t('features')}</div>
            <div className={classes.fiturTwo}>{t('pricing')}</div>
            <div className={classes.fiturThree}>{t('resources')}</div>
          </div>

          <div className={classes.language}>
            <Menu>
              <MenuButton>
                <div className={classes.menuBahasa}>
                  {selectedLanguage === 'en' ? (
                    <div className={classes.imgBahasa}>
                      <img src={usFlag} alt="English Flag" />
                    </div>
                  ) : (
                    <div className={classes.imgBahasa}>
                      <img src={idFlag} alt="Indonesian Flag" />
                    </div>
                  )}
                </div>
              </MenuButton>
              <MenuList className={classes.listLanguage}>
                <MenuItem onClick={() => handleLanguageChange('en')} className={classes.itemBahasa}>
                  English
                </MenuItem>
                <div>
                  <hr />
                </div>
                <MenuItem onClick={() => handleLanguageChange('id')} className={classes.itemBahasa}>
                  Indonesia
                </MenuItem>
              </MenuList>
            </Menu>
          </div>

          <div className={classes.navbarLogin}>
            <div className={classes.login}>{t('login')}</div>
            <div className={classes.daftar}>
              <Button variant="contained">{t('signup')}</Button>
            </div>
          </div>
        </div>

        <div className={classes.toggleMobile}>
          <div className={classes.language}>
            <Menu>
              <MenuButton>
                <div className={classes.menuBahasa}>
                  {selectedLanguage === 'en' ? (
                    <div className={classes.imgBahasa}>
                      <img src={usFlag} alt="English Flag" />
                    </div>
                  ) : (
                    <div className={classes.imgBahasa}>
                      <img src={idFlag} alt="Indonesian Flag" />
                    </div>
                  )}
                  <div className={classes.arrowBahasa}></div>
                </div>
              </MenuButton>
              <MenuList className={classes.listLanguage}>
                <MenuItem onClick={() => handleLanguageChange('en')} className={classes.itemBahasa}>
                  English
                </MenuItem>
                <div>
                  <hr />
                </div>
                <MenuItem onClick={() => handleLanguageChange('id')} className={classes.itemBahasa}>
                  Indonesia
                </MenuItem>
              </MenuList>
            </Menu>
          </div>
          <div className={classes.icon} onClick={handleToggle}>
            {!toggleNavbar ? <ReorderIcon /> : <CloseIcon />}
          </div>
        </div>
      </div>

      {isMobileOpen && (
        <div className={classes.mobile}>
          <div className={classes.fitur}>
            <div>{t('features')}</div>
            <div>{t('pricing')}</div>
            <div>{t('resources')}</div>
          </div>

          <hr className={classes.line} />
          <div className={classes.masuk}>
            <div>{t('login')}</div>
            <div>
              <Button className={classes.daftarMobile} variant="contained">
                {t('signup')}
              </Button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

Navbar.propTypes = {
  toggleNavbar: PropTypes.bool,
};

const mapStateToProps = createStructuredSelector({
  toggleNavbar: selectToggleNavbar,
});

export default connect(mapStateToProps)(Navbar);
