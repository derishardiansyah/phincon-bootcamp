import classes from './style.module.scss';
import facebook from '@static/images/icon-facebook.svg';
import twitter from '@static/images/icon-twitter.svg';
import pinterest from '@static/images/icon-pinterest.svg';
import instagram from '@static/images/icon-instagram.svg';
import { useTranslation } from 'react-i18next';

const Footer = () => {
  const { t } = useTranslation();
  return (
    <div className={classes.container}>
      <div className={classes.footer}>
        <div className={classes.nameFooter}>Shortly</div>
        <div className={classes.footerMid}>
          <div className={classes.feature}>
            <div>{t('features')}</div>
            <div className={classes.featureList}>
              <div>{t('link-shortening')}</div>
              <div>{t('brand')}</div>
              <div>{t('analytics')}</div>
            </div>
          </div>
          <div className={classes.resources}>
            <div>{t('resources')}</div>
            <div className={classes.featureList}>
              <div>{t('blog')}</div>
              <div>{t('developer')}</div>
              <div>{t('support')}</div>
            </div>
          </div>
          <div className={classes.company}>
            <div>{t('company')}</div>
            <div className={classes.featureList}>
              <div>{t('about')}</div>
              <div>{t('our')}</div>
              <div>{t('career')}</div>
              <div>{t('contact')}</div>
            </div>
          </div>
        </div>
        <div className={classes.social}>
          <img src={facebook} />
          <img src={twitter} />
          <img src={pinterest} />
          <img src={instagram} />
        </div>
      </div>
    </div>
  );
};

export default Footer;
