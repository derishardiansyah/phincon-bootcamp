import { createSelector } from 'reselect';
import { initialState } from '@containers/Shortlink/reducer';

const selectShortLinkState = (state) => state.shorturl || initialState;

export const selectShortUrl = createSelector(selectShortLinkState, (state) => state.shortUrl);
export const selectShortUrlLoading = createSelector(selectShortLinkState, (state) => state.shortUrlLoading);
export const selectShortUrlError = createSelector(selectShortLinkState, (state) => state.shortUrlError);
