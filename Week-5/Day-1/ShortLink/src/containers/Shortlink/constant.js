export const GET_SHORT_URL = 'ShortUrl/GET_SHORT_URL';
export const SET_SHORT_URL = 'ShortUrl/SET_SHORT_URL';
export const SET_SHORT_URL_LOADING = 'ShortUrl/SET_SHORT_URL_LOADING';
export const SET_SHORT_URL_ERROR = 'ShortUrl/SET_SHORT_URL_ERROR';
export const DELETE_SHORT_URL = 'ShortUrl/DELETE_SHORT_URL';
