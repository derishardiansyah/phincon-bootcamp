import { SET_TOGGLE_NAVBAR } from './constant';

export const setToggleNavbar = (toggleNavbar) => ({
  type: SET_TOGGLE_NAVBAR,
  toggleNavbar,
});
