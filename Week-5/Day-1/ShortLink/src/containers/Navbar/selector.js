import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectToggleState = (state) => state.navbar || initialState;

export const selectToggleNavbar = createSelector(selectToggleState, (state) => state.toggleNavbar);
