import { produce } from 'immer';
import { SET_TOGGLE_NAVBAR } from './constant';

export const initialState = {
  toggleNavbar: false,
};

const navbarReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TOGGLE_NAVBAR:
        draft.toggleNavbar = action.toggleNavbar;
        break;
      default:
        return state;
    }
  });

export default navbarReducer;
