import { combineReducers } from 'redux';

import appReducer, { storedKey as storedAppState } from '@containers/App/reducer';
import languageReducer from '@containers/Language/reducer';
import navbarReducer from '@containers/Navbar/reducer';
import shortLinkReducer, { storedKey as storedShortState } from '@containers/Shortlink/reducer';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  app: { reducer: appReducer, whitelist: storedAppState },
  shorturl: { reducer: shortLinkReducer, whitelist: storedShortState },
};

const temporaryReducers = {
  language: languageReducer,
  navbar: navbarReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
