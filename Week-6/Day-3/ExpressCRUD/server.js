import express from "express";
import fs from "fs";

const app = express();
const port = 3000;
const data = JSON.parse(fs.readFileSync("./Database/db.json"));

app.use(express.json());

const loadPremier = () => {
  try {
    return data.premier_league_teams;
  } catch (error) {
    return [];
  }
};

app.get("/premierleague/", (req, res) => {
  try {
    res.send(loadPremier());
    res.status(200);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.get("/premierleague/:nama_tim", (req, res) => {
  try {
    const requestedTeam = data.premier_league_teams.find(
      (team) =>
        team.nama_tim.toLowerCase() === req.params.nama_tim.toLowerCase()
    );
    if (requestedTeam) {
      res.json(requestedTeam);
    } else {
      res.status(404).json({
        message: "Tim tidak ditemukan",
      });
    }
  } catch (error) {
    res.status(404).json({
      message: "Internal server error",
    });
  }
});

app.get("/premierleague/city/:kota", (req, res) => {
  try {
    console.log(data.premier_league_teams);
    const requestedKota = data.premier_league_teams.filter(
      (team) => team.kota.toLowerCase() === req.params.kota.toLowerCase()
    );
    if (requestedKota) {
      res.json(requestedKota);
    } else {
      res.status(404).json({
        message: "kota tidak ditemukan",
      });
    }
  } catch (error) {
    res.status(404).json({
      message: "Tidak ada tim berdasarkan kota",
    });
  }
});

app.get("/premierleague/tahun/:tahun", (req, res) => {
  try {
    const requestedTahun = data.premier_league_teams.filter(
      (team) => team.tahun_berdiri === parseInt(req.params.tahun)
    );

    if (requestedTahun) {
      res.json(requestedTahun);
    } else {
      res.status(404).json({
        message: "tahun tidak ditemukan",
      });
    }
  } catch (error) {
    res.status(404).json({
      message: "Tidak ada tim berdasarkan tahun",
    });
  }
});

app.get("/premierleague/stadion/:stadion", (req, res) => {
  try {
    const requestedStadion = data.premier_league_teams.filter(
      (team) => team.stadion.toLowerCase() === req.params.stadion.toLowerCase()
    );
    if (requestedStadion) {
      res.json(requestedStadion);
    } else {
      res.status(404).json({
        message: "stadion tidak ditemukan",
      });
    }
  } catch (error) {
    res.status(404).json({
      message: "Tidak ada tim berdasarkan stadion",
    });
  }
});

app.post("/premierleague/", (req, res) => {
  try {
    const newTeam = req.body;
    const premierleague = loadPremier();
    const teamExists = premierleague.some(
      (team) => team.nama_tim.toLowerCase() === newTeam.nama_tim.toLowerCase()
    );
    if (teamExists) {
      return res
        .status(400)
        .json({ message: "Team with the same name already exists." });
    }
    premierleague.push(newTeam);
    fs.writeFileSync(
      "./Database/db.json",
      JSON.stringify({ premier_league_teams: premierleague }, null, 2)
    );
    res.status(201).json(newTeam);
  } catch (error) {
    res.status(500).json({ message: "Error adding team." });
  }
});

app.delete("/premierleague/:nama/", (req, res) => {
  try {
    const premierleagueDelete = loadPremier();
    const teamNameToDelete = req.params.nama.toLowerCase();

    const teamToDelete = premierleagueDelete.find(
      (team) => team.nama_tim.toLowerCase() === teamNameToDelete
    );
    if (!teamToDelete) {
      return res.status(404).json({ message: "Team not found." });
    }
    const filterDelete = premierleagueDelete.filter((premier) => {
      return premier.nama_tim.toLowerCase() !== teamNameToDelete;
    });
    fs.writeFileSync(
      "./Database/db.json",
      JSON.stringify({ premier_league_teams: filterDelete }, null, 2)
    );
    res.status(200).json({ message: "Data berhasil dihapus" });
  } catch (error) {
    res.status(500).json({ message: "Error deleting team." });
  }
});

app.put("/premierleague/:nama_tim", (req, res) => {
  try {
    const premierleague = loadPremier();
    const teamNameToUpdate = req.params.nama_tim.toLowerCase();
    const teamToUpdateIndex = premierleague.findIndex(
      (team) => team.nama_tim.toLowerCase() === teamNameToUpdate
    );
    if (teamToUpdateIndex === -1) {
      return res.status(404).json({ message: "Team not found." });
    }
    const otherTeamsWithSameName = premierleague.filter(
      (team, index) =>
        index !== teamToUpdateIndex &&
        team.nama_tim.toLowerCase() === teamNameToUpdate
    );
    if (otherTeamsWithSameName.length > 0) {
      return res.status(400).json({ message: "Team name is already in use." });
    }
    premierleague[teamToUpdateIndex] = {
      ...premierleague[teamToUpdateIndex],
      ...req.body,
    };
    fs.writeFileSync(
      "./Database/db.json",
      JSON.stringify({ premier_league_teams: premierleague }, null, 2)
    );
    res.status(200).json({ message: "Data berhasil diedit" });
  } catch (error) {
    res.status(500).json({ message: "Error updating team." });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
