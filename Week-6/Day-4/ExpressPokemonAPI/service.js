import express from "express";
import router from "./Routes/route.js";

const app = express();
const port = 3000;

app.use(express.json());
app.use("/pokemon", router);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
