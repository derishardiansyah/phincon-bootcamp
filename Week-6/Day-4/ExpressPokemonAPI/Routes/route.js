import { Router } from "express";
import API from "../API/api.js";

const router = Router();

router.get("/", API.getAll);
router.get("/apiPokemon/:id", API.getID);
router.get("/apiPokemon/:name", API.getName);
router.post("/newPokemon", API.createPokemon);
router.get("/newPokemon", API.getPokemon);
router.get("/newPokemon/release", API.getRelease);
router.put("/newPokemon/rename/:id", API.putRename);

export default router;
