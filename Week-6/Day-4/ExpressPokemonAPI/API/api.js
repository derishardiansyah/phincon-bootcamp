import axios from "axios";
import { readFileSync, writeFileSync } from "fs";
import { randomBytes } from "crypto";

const dbPath = new URL("../Database/pokemon.json", import.meta.url);
const readData = JSON.parse(readFileSync(dbPath, "utf-8"));
const writeData = (data) =>
  writeFileSync(dbPath, JSON.stringify(data, null, 2));

const API = {
  getAll: async (req, res) => {
    try {
      const response = await axios.get("https://pokeapi.co/api/v2/pokemon/");
      res.json(response.data.results);
    } catch (error) {
      res.status(500).json({ message: "Failed to get pokemon" });
    }
  },
  getID: async (req, res) => {
    const getid = req.params.id;
    try {
      const response = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${getid}`
      );
      res.json(response.data);
    } catch (error) {
      res.status(500).json({ message: "Failed to get ID pokemon" });
    }
  },
  getName: async (req, res) => {
    const getname = req.params.name;
    try {
      const response = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${getname}`
      );
      res.json(response.data);
    } catch (error) {
      res.status(500).json({ message: "Failed to get name pokemon" });
    }
  },
  createPokemon: async (req, res) => {
    try {
      const generatePokemonId = () => randomBytes(16).toString("hex");
      const data = readData;
      const existingPokemon = data.pokemon.find(
        (pokemon) => pokemon.name === data.name
      );
      if (existingPokemon) {
        res
          .status(400)
          .json({ message: "Pokemon with the same name already exists" });
      } else {
        const probability = Math.random();
        if (probability > 0.5) {
          const newData = {
            id: generatePokemonId(),
            name: req.body.name,
            type: req.body.type,
            release: 0,
          };
          writeData({ pokemon: [...data.pokemon, newData] });
          res.json({ message: "Pokemon created successfully" });
        } else {
          res.status(400).json({ message: "Pokemon not created" });
        }
      }
    } catch (error) {
      console.error("Error creating pokemon:", error);
      res.status(500).json({ message: "Failed to create pokemon" });
    }
  },

  getPokemon: async (req, res) => {
    try {
      const data = readData;
      res.json(data.pokemon);
    } catch (error) {
      console.error("Error getting pokemon:", error);
      res.status(500).json({ message: "Failed to get new pokemon" });
    }
  },
  getRelease: async (req, res) => {
    try {
      const checkPrime = (number) => {
        if (isNaN(number) || !isFinite(number) || number % 1 || number < 2) {
          return false;
        }
        const sqrtNumber = Math.sqrt(number);
        for (let i = 2; i <= sqrtNumber; i++) {
          if (number % i === 0) {
            return false;
          }
        }
        return true;
      };
      const release = Math.floor(Math.random() * 100) + 1;
      if (checkPrime(release)) {
        res.status(200).json({ message: "Pokemon released" });
      } else {
        res.status(400).json({ message: "Pokemon not release" });
      }
    } catch (error) {
      res.status(500).json({ message: "Failed to release pokemon" });
    }
  },
  putRename: async (req, res) => {
    try {
      const fibonacci = (index) => {
        if (index === 0) return 0;
        if (index === 1) return 1;

        let prev = 0;
        let current = 1;

        for (let i = 2; i <= index; i++) {
          const next = prev + current;
          prev = current;
          current = next;
        }

        return current;
      };
      const renamePokemon = (name, release) => {
        const splitName = name.split("-");
        const originalName = splitName[0];
        const fiboKey = fibonacci(release);
        return `${originalName}-${fiboKey}`;
      };
      const data = readData;
      const dataPokemon = data.pokemon;
      const selectedPokemon = dataPokemon.find(
        (item) => item.id === req.params.id
      );
      if (selectedPokemon) {
        const newPokemon = dataPokemon.map((item) => {
          if (item.id === req.params.id) {
            item.name = renamePokemon(item.name, item.release);
            item.release += 1;
          }
          return item;
        });
        writeData({ pokemon: newPokemon }, null, 2);
        res.json(newPokemon);
      }
      res.status(200).json({ message: "edit successfully" });
    } catch (error) {
      res.status(500).json({ message: "Failed to rename pokemon" });
    }
  },
};

export default API;
