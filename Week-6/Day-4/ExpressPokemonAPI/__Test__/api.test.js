import axios from 'axios';
import API from '../API/api'; // Path to your controller
import { readFileSync, writeFileSync } from 'fs';
import { randomBytes } from 'crypto';

jest.mock('axios');
jest.mock('fs');
jest.mock('crypto');

jest.mock('fs', () => ({
  readFileSync: jest.fn(() => JSON.stringify({ pokemon: [] })),
  writeFileSync: jest.fn(),
}));

describe('API', () => {
  let mockReq;
  let mockRes;

  beforeEach(() => {
    mockReq = {
      params: {},
      body: {},
    };
    mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };
    jest.clearAllMocks();
  });

  describe('getAll', () => {
    it('should get a list of pokemons', async () => {
      const mockData = {
        data: {
          results: [{ name: 'pikachu' }, { name: 'charmander' }],
        },
      };

      axios.get.mockResolvedValue(mockData);

      await API.getAll(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/');
    });

    it('should handle error when getting pokemons', async () => {
      axios.get.mockRejectedValueOnce(new Error('Network Error'));

      await API.getAll(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/');
    });
  });

  describe('getID', () => {
    let mockReq, mockRes;

    beforeEach(() => {
      mockReq = { params: {} };
      mockRes = {
        json: jest.fn(),
        status: jest.fn().mockReturnThis(),
      };
    });

    it('should get pokemon detail', async () => {
      const mockData = {
        data: {
          name: 'bulbasaur',
          height: 7,
          weight: 69,
        },
      };

      axios.get.mockResolvedValue(mockData);
      mockReq.params.id = '1';

      await API.getID(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/1');
      expect(mockRes.json).toHaveBeenCalledWith(mockData.data);
    });

    it('should handle not found and return 400', async () => {
      const mockError = { response: { status: 404 } };
      axios.get.mockRejectedValue(mockError);
      mockReq.params.id = 'Missing'; // Set the ID parameter

      await API.getID(mockReq, mockRes);
      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/Missing');
      expect(mockRes.status).toHaveBeenCalledWith(500);
      expect(mockRes.json).toHaveBeenCalledWith({ message: 'Failed to get ID pokemon' });
    });

    it('should handle error when getting pokemon detail', async () => {
      axios.get.mockRejectedValueOnce(new Error('Network Error'));
      mockReq.params.id = '1';

      await API.getID(mockReq, mockRes);

      expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon/1');
      expect(mockRes.status).toHaveBeenCalledWith(500);
      expect(mockRes.json).toHaveBeenCalledWith({ message: 'Failed to get ID pokemon' });
    });
  });

  describe('createPokemon', () => {
    let mockReq, mockRes;

    beforeEach(() => {
      mockReq = {
        body: {},
      };
      mockRes = {
        json: jest.fn(),
        status: jest.fn().mockReturnThis(),
      };
    });
    it('should create a new Pokemon successfully', async () => {
      const mockData = {
        pokemons: [{ id: '3', name: 'pikachu', type: 'metal', release: 0 }],
      };

      mockReq = {
        body: { name: 'pikachu', type: 'metal' },
      };
      mockReq.body = { name: 'pikachu', type: 'metal' };
      randomBytes.mockReturnValue({ toString: jest.fn().mockReturnValue('3') });
      Math.random = jest.fn().mockReturnValue(0.7);

      await API.createPokemon(mockReq, mockRes);

      const expectedMockData = {
        pokemons: [{ id: '3', name: 'pikachu', type: 'metal', release: 0 }],
      };

      const expectedPath = expect.stringContaining('../Database/pokemon.json');
      expect(writeFileSync).toHaveBeenCalledWith(expectedPath, JSON.stringify(expectedMockData, null, 2));
      expect(mockRes.json).toHaveBeenCalledWith({ message: 'Pokemon created successfully' });
    });

    // it('should handle adding an existing Pokemon', async () => {
    //   const mockData = {
    //     pokemons: [
    //       { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
    //       { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
    //     ],
    //   };

    //   mockReq = {
    //     body: { name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '10' },
    //   };

    //   mockReq.body = { name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '10' };
    //   readFileSync.mockReturnValue(JSON.stringify(mockData));
    //   Math.random = jest.fn().mockReturnValue(0.7);

    //   await API.createPokemon(mockReq, mockRes);

    //   expect(writeFileSync).not.toHaveBeenCalled();
    //   expect(mockRes.status).toHaveBeenCalledWith(400);
    //   expect(mockRes.json).toHaveBeenCalledWith({ message: 'Pokemon with the same name already exists' });
    // });

    // it('should handle failure to add a new Pokemon', async () => {
    //   const mockData = {
    //     pokemons: [
    //       { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
    //       { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
    //     ],
    //   };

    //   mockReq = {
    //     body: { name: 'pikachu', nameEvolusi: 'pikachu', height: '10' },
    //   };

    //   mockReq.body = { name: 'pikachu', nameEvolusi: 'pikachu', height: '10' };
    //   readFileSync.mockReturnValue(JSON.stringify(mockData));
    //   randomBytes.mockReturnValue({ toString: jest.fn().mockReturnValue('3') });
    //   Math.random = jest.fn().mockReturnValue(0.4);

    //   await API.createPokemon(mockReq, mockRes);

    //   // Expectations to verify failure to create
    //   expect(writeFileSync).not.toHaveBeenCalled();
    //   expect(mockRes.status).toHaveBeenCalledWith(400);
    //   expect(mockRes.json).toHaveBeenCalledWith({ message: 'Pokemon not created' });
    // });

    // it('should handle internal server error', async () => {
    //   readData.mockImplementation(() => {
    //     throw new Error('Some error');
    //   });

    //   await API.createPokemon(mockReq, mockRes);

    //   // Expectations to verify internal server error
    //   expect(mockRes.status).toHaveBeenCalledWith(500);
    //   expect(mockRes.json).toHaveBeenCalledWith({ message: 'Failed to create pokemon' });
    // });
  });

  // describe('getMyPokemon', () => {
  //   it('should get my Pokemon list successfully', async () => {
  //     const mockData = {
  //       pokemons: [
  //         { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
  //         { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
  //       ],
  //     };
  //     getData.mockReturnValue(mockData);

  //     API.getMyPokemon(mockReq, mockRes);

  //     expect(getData).toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, mockData.pokemons, 'Success to get pokemon');
  //   });

  //   it('should handle error and respond with 500', async () => {
  //     getData.mockImplementation(() => {
  //       throw new Error();
  //     });

  //     API.getMyPokemon(mockReq, mockRes);

  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
  //   });
  // });

  // describe('getMyPokemonPrime', () => {
  //   it('should release a Pokemon successfully if random number is prime', async () => {
  //     const mockData = {
  //       pokemons: [
  //         { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
  //         { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
  //       ],
  //     };
  //     getData.mockReturnValue(mockData);
  //     isPrime.mockReturnValue(true);

  //     API.getMyPokemonPrime(mockReq, mockRes);

  //     // Assert
  //     expect(getData).toHaveBeenCalled();
  //     expect(isPrime).toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, mockData, 'Success to release pokemon');
  //   });

  //   it('should fail to release a Pokemon if random number is not prime', async () => {
  //     const mockData = {
  //       pokemons: [
  //         { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
  //         { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
  //       ],
  //     };
  //     getData.mockReturnValue(mockData);
  //     isPrime.mockReturnValueOnce(false);

  //     API.getMyPokemonPrime(mockReq, mockRes);

  //     // Assert
  //     expect(getData).toHaveBeenCalled();
  //     expect(isPrime).toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, null, `Failed to release pokemon`);
  //   });

  //   it('should handle errors by sending a 500 response', async () => {
  //     getData.mockImplementation(() => {
  //       throw new Error('Some error');
  //     });

  //     API.getMyPokemonPrime(mockReq, mockRes);

  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
  //   });
  // });

  // describe('getMyPokemonPrimeNext', () => {
  //   const mockData = {
  //     pokemons: [
  //       { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
  //       { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
  //     ],
  //   };
  //   it('should call next middleware if random number is prime and Pokemon name is found', async () => {
  //     getData.mockReturnValue(mockData);
  //     isPrime.mockReturnValue(true);

  //     mockReq = {
  //       params: { name: 'bulbasaur' },
  //     };

  //     API.getMyPokemonPrimeNext(mockReq, mockRes, mockNext);

  //     // Pengecekan
  //     expect(getData).toHaveBeenCalled();
  //     expect(isPrime).toHaveBeenCalled();
  //     expect(mockNext).toHaveBeenCalled();
  //   });

  //   it('should send a response with a message if random number is not prime and Pokemon name is found', async () => {
  //     getData.mockReturnValue(mockData);
  //     isPrime.mockReturnValueOnce(false);
  //     mockReq = {
  //       params: { name: 'bulbasaur' },
  //     };

  //     API.getMyPokemonPrimeNext(mockReq, mockRes, mockNext);

  //     // Pengecekan
  //     expect(getData).toHaveBeenCalled();
  //     expect(isPrime).toHaveBeenCalled();
  //     expect(mockNext).not.toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 200, null, 'Failed to release pokemon');
  //   });

  //   it('should send a response with a message if Pokemon name is not found', async () => {
  //     getData.mockReturnValue(mockData);
  //     mockReq = {
  //       params: { name: 'nonExistentName' },
  //     };

  //     const { name } = mockReq.params;
  //     API.getMyPokemonPrimeNext(mockReq, mockRes, mockNext);

  //     expect(getData).toHaveBeenCalled();
  //     expect(isPrime).not.toHaveBeenCalled();
  //     expect(mockNext).not.toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 400, null, `cant find pokemon with name: ${name}`);
  //   });

  //   it('should handle errors by sending a 500 response', async () => {
  //     getData.mockImplementation(() => {
  //       throw new Error('Some error');
  //     });

  //     API.getMyPokemonPrimeNext(mockReq, mockRes);

  //     expect(getData).toHaveBeenCalled();
  //     expect(mockNext).not.toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
  //   });
  // });

  // describe('getMyPokemonRename', () => {
  //   const mockData = {
  //     pokemons: [
  //       { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur', height: '3', release: 0 },
  //       { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
  //     ],
  //   };

  //   it('should rename a Pokemon successfully', async () => {
  //     getData.mockReturnValue(mockData);

  //     mockReq = {
  //       params: { name: 'bulbasaur' },
  //     };
  //     renamePokemon.mockImplementation((nameEvolusi) => `${nameEvolusi}-0`);

  //     API.getMyPokemonRename(mockReq, mockRes);

  //     const expectedMockData = {
  //       pokemons: [
  //         { id: 1, name: 'bulbasaur', nameEvolusi: 'bulbasaur-0', height: '3', release: 1 },
  //         { id: 2, name: 'charmander', nameEvolusi: 'charmander', height: '8', release: 0 },
  //       ],
  //     };

  //     // Check
  //     expect(getData).toHaveBeenCalled();
  //     expect(setData).toHaveBeenCalledWith(expectedMockData);
  //     expect(renamePokemon).toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(
  //       mockRes,
  //       200,
  //       {
  //         id: 1,
  //         name: 'bulbasaur',
  //         nameEvolusi: 'bulbasaur-0',
  //         height: '3',
  //         release: 1,
  //       },
  //       `Success rename pokemon`
  //     );
  //   });

  //   it('should send a response with status 500 if an error occurs', async () => {
  //     getData.mockImplementation(() => {
  //       throw new Error('Some error');
  //     });

  //     await API.getMyPokemonRename(mockReq, mockRes);

  //     expect(getData).toHaveBeenCalled();
  //     expect(setData).not.toHaveBeenCalled();
  //     expect(renamePokemon).not.toHaveBeenCalled();
  //     expect(responseHelper).toHaveBeenCalledWith(mockRes, 500);
  //   });
  // });
});
