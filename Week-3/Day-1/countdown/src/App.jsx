import footerfacebook from "./assets/images/icon-facebook.svg";
import footerpinterest from "./assets/images/icon-pinterest.svg";
import footerinstagram from "./assets/images/icon-instagram.svg";
import Countdown from "react-countdown";
import "./App.css";
import { useSearchParams } from "react-router-dom";
import { useRef } from "react";

function App() {
  // menagtur date future
  const defaultDate = "2024-01-01:00:00";
  // example hook pada react-router-dom
  const [searchParams] = useSearchParams();
  // mengambil parameter future date
  const date = searchParams.get("date");
  // condiitonal date
  const dateRef = useRef(new Date(date || defaultDate));

  const renderer = ({ days, hours, minutes, seconds }) => {
    return (
      <div className="content">
        <div className="content-box">
          <div className="box">
            <div className="box-top"></div>
            <h3 className="countdown-box">{days}</h3>
            <div className="box-bottom"></div>
          </div>
          <div className="title">DAYS</div>
        </div>
        <div className="content-box">
          <div className="box">
            <div className="box-top"></div>
            <h3 className="countdown-box">{hours}</h3>
            <div className="box-bottom"></div>
          </div>
          <div className="title">HOURS</div>
        </div>
        <div className="content-box">
          <div className="box">
            <div className="box-top"></div>
            <h3 className="countdown-box">{minutes}</h3>
            <div className="box-bottom"></div>
          </div>
          <div className="title">MINUTES</div>
        </div>
        <div className="content-box">
          <div className="box">
            <div className="box-top"></div>
            <h3 className="countdown-box">{seconds}</h3>
            <div className="box-bottom"></div>
          </div>
          <div className="title">SECONDS</div>
        </div>
      </div>
    );
  };
  return (
    <div className="container">
      <div className="header">
        <h2 className="title-header">WE'RE LAUNCHING SOON</h2>
      </div>
      {/* current = memanggil date yang sekarang atau bisa dibilang tipe data pada useSearchParams */}
      <Countdown date={dateRef.current} renderer={renderer} />

      <div className="footer">
        <div className="footer-icon">
          <img src={footerfacebook} alt="" />
          <img src={footerpinterest} alt="" />
          <img src={footerinstagram} alt="" />
        </div>
      </div>
    </div>
  );
}

export default App;
