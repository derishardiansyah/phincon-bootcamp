import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
// untuk import memnaggil syntack client
import { createBrowserRouter, RouterProvider } from "react-router-dom";

// membuat jalur routing "/ harus wajib menggunakan"
const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* memanggil function router-dom */}
    <RouterProvider router={router} />
  </React.StrictMode>
);
