import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Navbar from "./component/Navbar/navbar";
import ContentUp from "./component/ContentHeader/contentheader";
import Content from "./component/Content/content";
import CountryDetail from "./component/CountryDetail/countrydetail";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";

const lightTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "light",
  },
});

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "dark",
  },
});

function App() {
  const [darkMode, setDarkMode] = useState(false);
  useEffect(() => {
    const rootElement = document.getElementById("root");
    if (rootElement) {
      if (darkMode) {
        rootElement.classList.add("dark-theme");
      } else {
        rootElement.classList.remove("dark-theme");
      }
    }
  }, [darkMode]);

  const handleThemeToggle = () => {
    setDarkMode((prevDarkMode) => !prevDarkMode);
  };

  return (
    <Router>
      <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
        <CssBaseline />
        <Navbar darkMode={darkMode} handleThemeToggle={handleThemeToggle} />
        <Routes>
          <Route path="/" element={<ContentUp />} />
          <Route path="/countries" element={<Content />} />
          <Route path="/country/:countryName" element={<CountryDetail />} />
        </Routes>
      </ThemeProvider>
    </Router>
  );
}

export default App;
