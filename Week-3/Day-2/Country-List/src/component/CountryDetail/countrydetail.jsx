import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import classes from "./countrydetail.module.scss";
import Button from "@mui/material/Button";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Link } from "react-router-dom";
import { Box } from "@mui/material";
import Skeleton from "@mui/material/Skeleton";

const CountryDetail = () => {
  const { countryName } = useParams();
  const [countryData, setCountryData] = useState(null);
  const [isDarkMode, setIsDarkMode] = useState(false);

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/name/${countryName}`)
      .then((response) => response.json())
      .then((data) => {
        if (data && data?.length > 0) {
          setCountryData(data[0]);
        } else {
          setCountryData(null);
        }
      })
      .catch((error) => console.log(error));
  }, [countryName]);

  const lightModeButtonClass = isDarkMode
    ? classes.backDark
    : classes.backLight;

  if (!countryData) {
    return (
      <div className={classes.sidepages}>
        <div className={`${classes.back} ${lightModeButtonClass}`}>
          <Link to="/">
            <Button variant="outlined" startIcon={<ArrowBackIcon />}>
              Back
            </Button>
          </Link>
        </div>
        <div className={classes.content}>
          <Skeleton variant="rect" width={555} height={400} animation="wave" />
          <div className={classes.nameCountry}>
            <h1>
              <Skeleton animation="wave" width={200} />
            </h1>
            <div className={classes.description}>
              <div className={classes.leftDescription}>
                <Skeleton animation="wave" width={150} />

                <Skeleton animation="wave" width={100} />

                <Skeleton animation="wave" width={100} />

                <Skeleton animation="wave" width={150} />

                <Skeleton animation="wave" width={100} />
              </div>
              <div className={classes.rightDescription}>
                <Skeleton animation="wave" width={150} />

                <Skeleton animation="wave" width={150} />

                <Skeleton animation="wave" width={150} />
              </div>
            </div>
            <div className={classes.boxBorder}>
              <div className={classes.boxBorder}>
                <Box sx={{ boxShadow: 1 }}>
                  <div className={classes.border}>
                    <Skeleton animation="wave" width={100} />
                  </div>
                </Box>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  const {
    name,
    population,
    region,
    capital,
    flags,
    subregion,
    tld,
    languages,
    currencies,
    borders,
  } = countryData;

  return (
    <>
      <div className={classes.sidepages}>
        <div className={`${classes.back} ${lightModeButtonClass}`}>
          <Link to="/">
            <Button variant="outlined" startIcon={<ArrowBackIcon />}>
              Back
            </Button>
          </Link>
        </div>
        <div className={classes.content}>
          <div className={classes.flag}>
            <img src={flags.png} alt="" />
          </div>
          <div className={classes.nameCountry}>
            <h1>{name?.common ?? "Country Not Found"}</h1>
            <div className={classes.description}>
              <div className={classes.leftDescription}>
                <p>
                  Native Main :{" "}
                  {countryData?.name?.nativeName
                    ? Object.keys(countryData.name.nativeName)
                        .map((key) => countryData.name.nativeName[key].common)
                        .join(", ")
                    : "N/A"}
                </p>
                <p>Population: {population?.toLocaleString("id") ?? "N/A"}</p>
                <p>Region: {region ?? "N/A"}</p>
                <p>Sub Region: {subregion ?? "N/A"}</p>
                <p>Capital: {capital ?? "N/A"}</p>
              </div>
              <div className={classes.rightDescription}>
                <p>Top Level domain: {tld ?? "N/A"}</p>
                <p>
                  Currencies:{" "}
                  {currencies
                    ? currencies[Object.keys(currencies)[0]].name
                    : "N/A"}
                </p>
                <p>
                  Languages:{" "}
                  {languages ? Object.values(languages).join(", ") : "N/A"}
                </p>
              </div>
            </div>
            <div className={classes.boxBorder}>
              {borders && borders?.length > 0 && (
                <div className={classes.boxBorder}>
                  <p>Border Countries:</p>
                  {borders?.map((border) => (
                    <Box
                      key={border}
                      sx={{
                        boxShadow: 1,
                      }}
                    >
                      <div className={classes.border}>{border}</div>
                    </Box>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CountryDetail;
