import classes from "./navbar.module.scss";
import { Button } from "@mui/material";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

const Navbar = ({ darkMode, handleThemeToggle }) => {
  return (
    <div className={`${classes.navbar} ${darkMode ? classes.darkMode : ""}`}>
      <div
        className={`${classes.nameNavbar} ${
          darkMode ? classes.darkModeText : ""
        }`}
      >
        Where in the world?
      </div>
      <div className={classes.sideRight}>
        <Button
          variant="contained"
          startIcon={darkMode ? <Brightness7Icon /> : <Brightness4Icon />}
          onClick={handleThemeToggle}
          className={classes.button}
        >
          {darkMode ? "Light Mode" : "Dark Mode"}
        </Button>
      </div>
    </div>
  );
};

export default Navbar;
