import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea, Skeleton, Pagination } from "@mui/material";
import classes from "./content.module.scss";

const Content = ({ selectedRegion, searchQuery, sortType, toggleSort }) => {
  const [countries, setCountries] = useState([]);
  const [filteredCountries, setFilteredCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 8;

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        const sortedCountries = data.sort((a, b) =>
          a.name.common.localeCompare(b.name.common)
        );
        setCountries(sortedCountries);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    let filteredCountriesByRegion = countries;
    if (selectedRegion !== "All") {
      filteredCountriesByRegion = countries.filter(
        (country) => country.region === selectedRegion
      );
    }

    const filteredCountriesBySearch = filteredCountriesByRegion.filter(
      (country) =>
        country.name.common.toLowerCase().includes(searchQuery.toLowerCase())
    );

    const sortedCountries =
      sortType === "ascending"
        ? filteredCountriesBySearch.sort((a, b) =>
            a.name.common.localeCompare(b.name.common)
          )
        : filteredCountriesBySearch.sort((a, b) =>
            b.name.common.localeCompare(a.name.common)
          );

    setFilteredCountries(sortedCountries);
    setCurrentPage(1);
  }, [selectedRegion, searchQuery, countries, sortType]);

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filteredCountries.slice(
    indexOfFirstItem,
    indexOfLastItem
  );

  return (
    <div className={classes.content}>
      {loading ? (
        <>
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
          <Skeleton variant="rectangular" height={275} />
        </>
      ) : (
        <>
          {currentItems.map((country, index) => (
            <Link
              to={`/country/${country.name.common
                .replace(/\s+/g, "-")
                .toLowerCase()}`}
              key={index}
              style={{ textDecoration: "none" }}
            >
              <Card sx={{ maxWidth: 345 }}>
                <CardActionArea>
                  <CardMedia
                    component="img"
                    height="140"
                    image={country.flags.png}
                    alt={country.name.common}
                  />
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      <span className={classes.countryName}>
                        {country.name.common.toLowerCase()}
                      </span>
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h7"
                      component="div"
                      fontWeight="bold"
                    >
                      Population:{" "}
                      {country.population?.toLocaleString("id") ?? "N/A"}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h7"
                      component="div"
                      fontWeight="bold"
                    >
                      Region: {country.region ?? "N/A"}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h7"
                      component="div"
                      fontWeight="bold"
                    >
                      Capital: {country.capital ?? "N/A"}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          ))}
          <div className={classes.paginationContainer}>
            <Pagination
              count={Math.ceil(filteredCountries.length / itemsPerPage)}
              page={currentPage}
              onChange={handlePageChange}
              color="primary"
              className={classes.pagination}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default Content;
