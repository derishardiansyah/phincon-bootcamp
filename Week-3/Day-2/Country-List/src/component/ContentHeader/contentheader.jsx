import React, { useState } from "react";
import classes from "./contentheader.module.scss";
import { TextField } from "@mui/material";
import { styled, alpha } from "@mui/material/styles";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import Content from "../Content/content";
import SortIcon from "@mui/icons-material/Sort";

const StyledMenu = styled((props) => (
  <Menu
    elevation={0}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right",
    }}
    {...props}
  />
))(({ theme }) => ({
  "& .MuiPaper-root": {
    borderRadius: 6,
    marginTop: theme.spacing(1),
    minWidth: 180,
    color:
      theme.palette.mode === "light"
        ? "rgb(55, 65, 81)"
        : theme.palette.grey[300],
    boxShadow:
      "rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px",
    "& .MuiMenu-list": {
      padding: "4px 0",
    },
    "& .MuiMenuItem-root": {
      "& .MuiSvgIcon-root": {
        fontSize: 18,
        color: theme.palette.text.secondary,
        marginRight: theme.spacing(1.5),
      },
      "&:active": {
        backgroundColor: alpha(
          theme.palette.primary.main,
          theme.palette.action.selectedOpacity
        ),
      },
    },
  },
}));

const ContentHeader = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);
  const [selectedRegion, setSelectedRegion] = useState("All");
  const [searchQuery, setSearchQuery] = useState("");
  const [sortType, setSortType] = useState("ascending");

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    setOpen(true);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setOpen(false);
  };

  const handleFilterByRegion = (region) => {
    setSelectedRegion(region);
    handleClose();
  };

  const toggleSort = () => {
    setSortType((prevSortType) =>
      prevSortType === "ascending" ? "descending" : "ascending"
    );
  };

  return (
    <div className={classes.contentheader}>
      <div className={classes.inputSearch}>
        <TextField
          id="outlined-basic"
          label="Search for a country"
          variant="outlined"
          className={classes.search}
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        <div className={classes.rightHeader}>
          <Button
            id="demo-customized-button"
            aria-controls={open ? "demo-customized-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            variant="outlined"
            className={classes.color}
            disableElevation
            onClick={handleClick}
            endIcon={<KeyboardArrowDownIcon />}
          >
            Filter by Region
          </Button>
          <StyledMenu
            id="demo-customized-menu"
            MenuListProps={{
              "aria-labelledby": "demo-customized-button",
            }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={() => handleFilterByRegion("All")} disableRipple>
              All
            </MenuItem>
            <MenuItem
              onClick={() => handleFilterByRegion("Africa")}
              disableRipple
            >
              Africa
            </MenuItem>
            <MenuItem
              onClick={() => handleFilterByRegion("Americas")}
              disableRipple
            >
              America
            </MenuItem>
            <MenuItem
              onClick={() => handleFilterByRegion("Asia")}
              disableRipple
            >
              Asia
            </MenuItem>
            <MenuItem
              onClick={() => handleFilterByRegion("Europe")}
              disableRipple
            >
              Europe
            </MenuItem>
            <MenuItem
              onClick={() => handleFilterByRegion("Oceania")}
              disableRipple
            >
              Oceania
            </MenuItem>
          </StyledMenu>
          <Button
            id="sort-button"
            variant="outlined"
            className={classes.color}
            disableElevation
            onClick={toggleSort}
            startIcon={<SortIcon />}
          >
            {sortType === "ascending" ? "ASC" : "DESC"}
          </Button>
        </div>
      </div>
      <Content
        selectedRegion={selectedRegion}
        searchQuery={searchQuery}
        sortType={sortType}
        toggleSort={toggleSort}
      />
    </div>
  );
};

export default ContentHeader;
